const express = require('express')
const app = express()

let todos = [
    {
        name: 'Korn',
        id: 1
    },
    {
        name: 'Por',
        id: 2
    }
]

//SELECT *FROMTODO
app.get('/todos',(req,res)=>{
    res.send(todos)
})

// INSERT INTO TODO
app.post('/todos',(req,res)=>{
    let newTodo = {
        name: 'Read a book',
        id: 3
    }

    todos.push(newTodo)
    res.status(201).send()
})

app.listen(3000,()=>{
    console.log('TODO API Started at port 3000')
})